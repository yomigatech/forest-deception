#Forest of Deception
*** This game does not have all intended mechanics since we ran out of time.   Will update if possible. ***

##How to Play
You and two friends must make your way through the Forest of Deception to find the Fountain of Fortunes.

Your party must unanimously decide on a direction to travel.  Each character has a specific comfort item that they need:
* Nobleman - Gold Pieces
* Lumberjack - Axes
* Bard - Poems
* Time Traveler - Time Serum
* Secret Character - Mysteries

As you travel to new areas, your comfort tokens will decrease, causing your Willpower meter to drain.  The less your character's comforts are met, the faster your Willpower will drain.  If a character's Willpower meter drains completely, they will be unable to continue with the group.

In each area, all characters will be required to confirm a direction of travel.  The party should come to an agreement on the direction to travel and lock in their votes by standing in the corresponding green zone and pushing the Confirm button.

On your journey, you may find items to replenish a character's specific comfort tokens.  Make sure to grab the item before moving on to the next area. 

###Win Condition
* Find the Fountain of Fortunes

###Lose Conditions
* If 2/3 party members lose their Willpower, the quest will be a failure.
* If you encounter the Witches' Hovel

##Platforms
Linux
Mac

##To Do

###Elisa
* Add comfort token tick down after each map transition
* Add comfort token replenish effect
* Spawn characters in corresponding entrance based on the last map's exit
* Fix confirming bug
* Add one player keyboard support for debugging and for demo purposes when there is no controller available
* Make player selection bubbles larger
* Add comfort and danger thought bubble mechanics
* Add multiple of same items per map tile
* Scale thought bubble icon based on probability
* Make thought bubbles determine potential next map's resources
* Add lose and win handling
* Generate fountain and witches map in each game
* Restrict movement so that players cannot go outside the game window
* Don't allow users to move back to the tile they came from
* Decide how to handle map (jit randomized, predetermined, etc)
* Add/discuss influence mechanics

###Yoran
* Improve variety of forest tiles
* Create forest objects like rocks, trees, grass, rotted logs, etc
* Animated forest tiles?
* Improve visibility and scale of pickup items
* Recreate maps with less water bodies and replace with rivers and streams
* Make comfort and danger thought bubbles sprites
* Create witches tilemap
* Create witches layout
* Create fountain tilemap
* Create fountain layout
* Create walk animations for all characters
* Add better music to game
* Improve game title image
* Port Sprite Edit project files to Aseprite
* Add arcade cabinet background to game window
* Add concise instructions on left game panel of arcade cabinet
* Update GGJ page when game mechanics are done
* Finish creating media and metadata about game on GGJ site (Screencap, videos, etc)
* Send working game to IGDA organizer (vazzer)

##Credits
* Mark Schmelzenbach
* Elisa Marchione (yoran@yomigatech.com)
* Yoran Marchione (elisa@yomigatech.com)
