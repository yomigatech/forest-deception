You and two friends must make your way through the Forest of Deception to find the Fountain of Fortunes. 

Your party must unanimously decide on a direction to travel. Each character has a specific comfort item that they need: 
* Nobleman - Gold Pieces 
* Lumberjack - Axes 
* Bard - Poems 
* Time Traveler - Time Serum 
* Secret Character - Mysteries 

As you travel to new areas, your comfort tokens will decrease, causing your Willpower meter to drain. The less your character's comforts are met, the faster your Willpower will drain. If a character's Willpower meter drains completely, they will be unable to continue with the group. In each area, all characters will be required to confirm a direction of travel. The party should come to an agreement on the direction to travel and lock in their votes by standing in the corresponding green zone and pushing the Confirm button. On your journey, you may find items to replenish a character's specific comfort tokens. Make sure to grab the item before moving on to the next area. 
Win Condition: 
* Find the Fountain of Fortunes 
Lose Conditions: 
* If any party member loses their Willpower, the quest will be a failure.
