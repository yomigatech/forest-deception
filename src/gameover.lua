local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'

local GameOver = Game:addState("GameOver")

function GameOver:enteredState() -- create buttons, options, etc and store them into self
  -- load close win screen here
end

function GameOver:draw() -- draw the menu
  love.graphics.setFont(menuFont) 
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Game Over",0,100,1280,'center')
  
  love.graphics.printf("Disillusioned and despondant..",0,200,1280,'center')
  -- display sad screen graphic
  love.graphics.printf("Each goes their own way.",0,240,1280,'center')
  love.graphics.setFont(smallFont)
  love.graphics.setColor(0,0,0)
  love.graphics.printf("Press B button to try again",0,660,1280,'center')
  love.graphics.setFont(menuFont)
end

function GameOver:update(dt) -- update anything that needs updates
  if (p1) then
    if (self.p1Back) and (p1:isGamepadDown('b')==false) then
      playSound(menuBack)
      self:gotoState("Menu")
    elseif (p1:isGamepadDown('b')) then
      self.p1Back=true
    end
  end
end

function GameOver:exitedState() -- destroy buttons, options etc here
  self.p1Back=false
end

function GameOver:keypressed(key, code)
  if (key == 'escape') then
    playSound(menuBack)
    self:gotoState("Menu")
  end
end

return GameOver