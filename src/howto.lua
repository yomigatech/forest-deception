local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
local Item = require 'item'

local HowTo = Game:addState("HowTo")

function HowTo:enteredState() -- create buttons, options, etc and store them into self
  self.p1Back=false
  self.items={}
  self.items[1]=Item:new(300/3,300/3,1)
  self.items[2]=Item:new(500/3,280/3,2)
  self.items[3]=Item:new(700/3,280/3,3)
  self.items[4]=Item:new(900/3,280/3,4)
  self.items[5]=Item:new(1100/3,280/3,5)
  self.screen=1
end

function HowTo:draw() -- draw the menu
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("How To Play",0,100,1280,'center')
  
  if (self.screen==1) then
    love.graphics.draw(controller,(self.screenWidth-self.title:getWidth())/2,200)
    love.graphics.setFont(smallFont)
    love.graphics.setColor(0,0,0)
    love.graphics.print("You and two friends must make your way through the Forest of Deception to find the Fountain of\nFortunes. In order to proceed, your party must unanimously decide on a direction to travel.  In each\narea, all characters will be required to confirm a direction of travel.  The party should come to an\nagreement on the direction to travel and lock in their votes by standing in the corresponding green zone\nand pushing the Confirm button.",240,440)
    
  love.graphics.setColor(192,255,192)  
  love.graphics.circle('fill',610,600,8)
  love.graphics.setColor(64,64,64)
  love.graphics.circle('fill',650,600,8)
  love.graphics.setColor(0,0,0)
  love.graphics.circle('line',610,600,8)
  love.graphics.circle('line',650,600,8)
  love.graphics.setColor(255,255,255)
  
  elseif (self.screen==2) then
    spriteSheets[1].anim[1]:draw(spriteSheets[1].image,260,276, 0, 3, 3, 6, 0)
    spriteSheets[2].anim[1]:draw(spriteSheets[2].image,460,250, 0, 3, 3, 6, 0)
    spriteSheets[3].anim[1]:draw(spriteSheets[3].image,660,264, 0, 3, 3, 6, 0)
    spriteSheets[4].anim[1]:draw(spriteSheets[4].image,860,270, 0, 3, 3, 6, 0)
    spriteSheets[5].anim[1]:draw(spriteSheets[5].image,1060,264, 0, 3, 3, 6, 0)
    love.graphics.push()
    love.graphics.scale(3,3)
    self.items[1]:draw() 
    self.items[2]:draw()
    self.items[3]:draw()
    self.items[4]:draw()
    self.items[5]:draw()
    love.graphics.pop()
    
    love.graphics.setFont(smallFont)
    love.graphics.setColor(0,0,0)
    love.graphics.print("Each character has a specific comfort item that they need:",240,200)
    love.graphics.print("As you travel to new areas, your comfort tokens will decrease, causing your Willpower meter to drain.\nThe less your character's comforts are met, the faster you will falter.  If a character's Willpower meter\ndrains completely, they will be unable to continue with the group.\n\nOn your journey, you may find items to replenish a character's specific comfort tokens. Make sure to\ngrab the item before moving on to the next area.",240,340) 
  
    love.graphics.setColor(255,255,255)
    love.graphics.printf("Find the Fountain of Fortunes.",0,500,1280,'center')
    love.graphics.printf("Beware, should any party member lose their Willpower, the quest will fail.",0,540,1280,'center')
  
    love.graphics.setColor(64,64,64)
    love.graphics.circle('fill',610,600,8)
    love.graphics.setColor(192,255,192)
    love.graphics.circle('fill',650,600,8)
    love.graphics.setColor(0,0,0)
    love.graphics.circle('line',610,600,8)
    love.graphics.circle('line',650,600,8)
    love.graphics.setColor(255,255,255)
  end
  love.graphics.setFont(menuFont)
end

function HowTo:update(dt) -- update anything that needs updates
  self.items[1]:update(dt)
  self.items[2]:update(dt)
  self.items[3]:update(dt)
  self.items[4]:update(dt)
  self.items[5]:update(dt)
  
  if (p1) then
    if (self.p1Back) and (p1:isGamepadDown('b')==false) then
      playSound(menuBack)
      self:popState()
    elseif (p1:isGamepadDown('b')) then
      self.p1Back=true
    end
    
    local leftx = p1:getGamepadAxis('leftx')
    if (leftx<-0.25) then
      self.screen=1
    elseif (leftx>0.25) then
      self.screen=2
    end
  end
end

function HowTo:exitedState() -- destroy buttons, options etc here
  self.p1Back=false
  items={}
end

function HowTo:keypressed(key, code)
  if (key == 'escape') then
    playSound(menuBack)
    self:popState()
  elseif (key=='a') or (key=='left') then
    self.screen=1
  elseif (key=='d') or (key=='right') then
    self.screen=2
  end
end

return HowTo