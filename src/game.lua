local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = class("Game"):include(Stateful)
local anim8 = require 'lib/anim8'

function playSound(sd)
  love.audio.newSource(sd,"static"):play()
end

function Game:initialize()
  
  menuFont = love.graphics.newFont("assets/linux_libertine/LinLibertine_RB.ttf", 32)
  smallFont = love.graphics.newFont("assets/linux_libertine/LinLibertine_RB.ttf", 18)
  
	self.title =  love.graphics.newImage("assets/title.png")
	player1 = { x=0, y=0, w=12, h=20, spriteIdx = 1, animIdx = 1, flipX = 1, animate=false, 
              name="Nobleman", alive=true}
	player2 = { x=0, y=0, w=12, h=20, spriteIdx = 2, animIdx = 1, flipX = 1, animate=false, 
              name="Lumberjack", alive=true}
  player3 = { x=0, y=0, w=12, h=20, spriteIdx = 3, animIdx = 1, flipX = 1, animate=false, 
              name="Bard", alive=true}
  
  -- graphics for character selection pointers
  pointers = { nil, nil, nil }
  pointers[1] = love.graphics.newImage("assets/pointer-p1.png")
  pointers[2] = love.graphics.newImage("assets/pointer-p2.png")
  pointers[3] = love.graphics.newImage("assets/pointer-p3.png")
  
  -- graphics for character selection portraits
  portraits = { nil, nil, nil, nil, nil }
  portraits[1] = love.graphics.newImage("assets/charFace_Nobleman.png")
  portraits[2] = love.graphics.newImage("assets/charFace_Lumberjack.png")
  portraits[3] = love.graphics.newImage("assets/charFace_Bard.png")
  portraits[4] = love.graphics.newImage("assets/charFace_TimeTraveller.png")
  portraits[5] = love.graphics.newImage("assets/charFace_Secret.png")
  
  spriteSheets = { 
    -- animation idle, right, front, back, death
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=12, h=20, item=nil, itemAnim=nil, itemW=8, itemH=10}, 
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=17, h=29, item=nil, itemAnim=nil, itemW=7, itemH=10},
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=11, h=24, item=nil, itemAnim=nil, itemW=9, itemH=10},
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=12, h=22, item=nil, itemAnim=nil, itemW=9, itemH=11},
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=14, h=23, item=nil, itemAnim=nil, itemW=10, itemH=15},
    {image=nil, anim={ nil, nil, nil, nil, nil }, w=0, h=0, item=nil, itemAnim=nil, itemW=11, itemH=11}}
  
  spriteSheets[1].image = love.graphics.newImage("assets/charSheet_Nobleman.png")
  spriteSheets[1].image:setFilter("nearest")
  local grid = anim8.newGrid(12,20,spriteSheets[1].image:getWidth(),spriteSheets[1].image:getHeight())
  spriteSheets[1].anim[1] = anim8.newAnimation(grid('1-6',1),0.05)
  spriteSheets[1].anim[2] = anim8.newAnimation(grid('1-6',1),0.05)
  spriteSheets[1].anim[3] = anim8.newAnimation(grid('1-6',1),0.05)
  spriteSheets[1].anim[4] = anim8.newAnimation(grid('1-6',1),0.05)
  spriteSheets[1].anim[5] = anim8.newAnimation(grid('1-6',1),0.05)
  spriteSheets[1].item = love.graphics.newImage("assets/itemSheet_Goldpiece.png")
  spriteSheets[1].item:setFilter("nearest")
  grid = anim8.newGrid(8,10,spriteSheets[1].item:getWidth(),spriteSheets[1].item:getHeight())
  spriteSheets[1].itemAnim = anim8.newAnimation(grid('1-6',1),0.2)
  
  spriteSheets[2].image = love.graphics.newImage("assets/charRef_Lumberjack.png")
  spriteSheets[2].image:setFilter("nearest")
  grid = anim8.newGrid(17,29,spriteSheets[2].image:getWidth(),spriteSheets[2].image:getHeight())
  spriteSheets[2].anim[1] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[2].anim[2] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[2].anim[3] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[2].anim[4] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[2].anim[5] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[2].item = love.graphics.newImage("assets/itemSheet_Axe.png")
  spriteSheets[2].item:setFilter("nearest")
  grid = anim8.newGrid(7,10,spriteSheets[2].item:getWidth(),spriteSheets[2].item:getHeight())
  spriteSheets[2].itemAnim = anim8.newAnimation(grid('1-6',1),0.2)
  
  spriteSheets[3].image = love.graphics.newImage("assets/charRef_Bard.png")
  spriteSheets[3].image:setFilter("nearest")
  grid = anim8.newGrid(11,24,spriteSheets[3].image:getWidth(),spriteSheets[3].image:getHeight())
  spriteSheets[3].anim[1] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[3].anim[2] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[3].anim[3] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[3].anim[4] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[3].anim[5] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[3].item = love.graphics.newImage("assets/itemSheet_Poem.png")
  spriteSheets[3].item:setFilter("nearest")
  grid = anim8.newGrid(9,10,spriteSheets[3].item:getWidth(),spriteSheets[3].item:getHeight())
  spriteSheets[3].itemAnim = anim8.newAnimation(grid('1-6',1),0.2)
  
  spriteSheets[4].image = love.graphics.newImage("assets/charRef_TimeTraveller.png")
  spriteSheets[4].image:setFilter("nearest")
  grid = anim8.newGrid(12,22,spriteSheets[4].image:getWidth(),spriteSheets[4].image:getHeight())
  spriteSheets[4].anim[1] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[4].anim[2] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[4].anim[3] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[4].anim[4] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[4].anim[5] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[4].item = love.graphics.newImage("assets/itemSheet_Plutonium.png")
  spriteSheets[4].item:setFilter("nearest")
  grid = anim8.newGrid(9,11,spriteSheets[4].item:getWidth(),spriteSheets[4].item:getHeight())
  spriteSheets[4].itemAnim = anim8.newAnimation(grid('1-6',1),0.2)
  
  spriteSheets[5].image = love.graphics.newImage("assets/charRef_Secret.png")
  spriteSheets[5].image:setFilter("nearest")
  grid = anim8.newGrid(14,23,spriteSheets[5].image:getWidth(),spriteSheets[5].image:getHeight())
  spriteSheets[5].anim[1] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[5].anim[2] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[5].anim[3] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[5].anim[4] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[5].anim[5] = anim8.newAnimation(grid(1,1),0.05)
  spriteSheets[5].item = love.graphics.newImage("assets/itemSheet_Question.png")
  spriteSheets[5].item:setFilter("nearest")
  grid = anim8.newGrid(10,15,spriteSheets[5].item:getWidth(),spriteSheets[5].item:getHeight())
  spriteSheets[5].itemAnim = anim8.newAnimation(grid('1-6',1),0.2)
  
  --character 6 is actually shroom only
  spriteSheets[6].item = love.graphics.newImage("assets/itemSheet_Shroom.png")
  spriteSheets[6].item:setFilter("nearest")
  grid = anim8.newGrid(11,11,spriteSheets[6].item:getWidth(),spriteSheets[6].item:getHeight())
  spriteSheets[6].itemAnim = anim8.newAnimation(grid(1,1),0.2)
  
  -- sounds for menu selection
  names = { nil, nil, nil, nil, nil }
  names[1] = love.sound.newSoundData("assets/sounds/nobleman.ogg")
  names[2] = love.sound.newSoundData("assets/sounds/lumberjack.ogg")
  names[3] = love.sound.newSoundData("assets/sounds/bard.ogg")
  names[4] = love.sound.newSoundData("assets/sounds/timetraveller.ogg")
  names[5] = love.sound.newSoundData("assets/sounds/unknown.ogg")
  
  -- sounds for directions
  direction = { {north=nil,east=nil,south=nil,west=nil}, {north=nil,east=nil,south=nil,west=nil} }
  direction[1].north = love.sound.newSoundData("assets/sounds/north-male1.ogg")
  direction[1].east = love.sound.newSoundData("assets/sounds/east-male1.ogg")
  direction[1].south = love.sound.newSoundData("assets/sounds/south-male1.ogg")
  direction[1].west = love.sound.newSoundData("assets/sounds/west-male1.ogg")
  
  direction[2].north = love.sound.newSoundData("assets/sounds/north-male1.ogg")
  direction[2].east = love.sound.newSoundData("assets/sounds/east-male1.ogg")
  direction[2].south = love.sound.newSoundData("assets/sounds/west-male1.ogg")
  direction[2].west = love.sound.newSoundData("assets/sounds/south-male1.ogg")
  
  -- graphics for how-to
  controller = love.graphics.newImage("assets/controller.png")
  
  -- some sounds
  menuMove = love.sound.newSoundData("assets/sounds/menuMove.ogg") 
  menuSelect = love.sound.newSoundData("assets/sounds/menuSelect.ogg")
  menuBack = love.sound.newSoundData("assets/sounds/menuBack.ogg")
  partyConfirm = love.sound.newSoundData("assets/sounds/partyConfirm.ogg")
  itemPickup = love.sound.newSoundData("assets/sounds/itemPickup.ogg")
  menuSong = love.audio.newSource("assets/sounds/introSong.ogg")
  menuSong:setVolume(0.4)
  ambient = love.audio.newSource("assets/sounds/ambientSong.ogg")
  ambient:setVolume(0.2)
  ambient:setLooping(true)

  -- get the list of joysticks
	local joysticks = love.joystick.getJoysticks()
  -- if not plugged in, these will be nil
	p1 = joysticks[1]
	p2 = joysticks[2]
  p3 = joysticks[3]
  
  -- number of players in game
  expectedPlayers=2
  
  -- if keyboard is used, it is this player, otherwise set to 0
  keyboardPlayer=0
  
  if (p1) and (p2) and (p3) then
    expectedPlayers=3
  end
  
	self:gotoState("Menu") -- start on the Menu state
end

function Game:update(dt)
end

return Game
