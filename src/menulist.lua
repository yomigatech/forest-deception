local class = require 'lib/middleclass'

-- BUTTON (internal class)
local Button = class('Button')

-- settings for all buttons
local width = 200
local height = 50
local spacing = 20 -- vertical spacing

-- private methods
local function getBoundingBox(self)
  local x = love.graphics.getWidth() / 2 - width / 2
  local y = 280 + (self.position - 1) * (height + spacing)

  return x,y,width,height
end

local function getFontYOffset(y)
  local font = love.graphics.getFont()
  if font then return y + (height / 2) - (font:getHeight() / 2) end
  return 0
end

-- public methods
function Button:initialize(position,label,callback)
  self.position = position
  self.label = label
  self.callback = callback
end

function Button:draw(selected)
  local x, y, width, height = getBoundingBox(self)
  if (selected) then
    love.graphics.setColor(32,128,32)
    love.graphics.rectangle('fill', x, y, width, height)
  end
  if (selected) then
    love.graphics.setColor(192,255,192)
  else
    love.graphics.setColor(255,255,255)
  end
  love.graphics.printf(self.label, x, getFontYOffset(y), width, 'center')
  
  love.graphics.setColor(255,255,255)
  love.graphics.rectangle('line', x, y, width, height)
end

function Button:activate()
  playSound(menuSelect)
  self.callback()
end

-- MENU
local MenuList = class('MenuList')

-- private methods
local function parseButtonOptions(self, buttonOptions)
  local options, label, callback
  self.maxButtons = 0
  for i=1, #buttonOptions do
    options = buttonOptions[i]
    label = options[1]
    callback = options[2]
    table.insert(self.buttons, Button:new(i, label, callback))
    self.maxButtons = self.maxButtons + 1
  end
end

--public functions
function MenuList:initialize(buttonOptions)
  self.buttons = {}
  parseButtonOptions(self, buttonOptions)
  self.selectedButton = 1
  self.changeCounter = 0
end

function MenuList:draw()
  for i=1, #self.buttons do
    self.buttons[i]:draw(self.selectedButton==i)
  end
end

function MenuList:update(dt)
  -- a bit weird here, since we are emulating key repeat
  local oldButton = self.selectedButton
  if (self.changeCounter>0) then
    self.changeCounter=self.changeCounter-dt
  else
    if (p1) then
      local lefty = p1:getGamepadAxis('lefty')
      if (lefty<-0.25) then
        self.selectedButton=self.selectedButton-1
      elseif (lefty>0.25) then
        self.selectedButton=self.selectedButton+1
      end
    end
  
    if (self.selectedButton<1) then
      self.selectedButton=1
    elseif (self.selectedButton>self.maxButtons) then
      self.selectedButton=self.maxButtons
    end
  
    if (self.selectedButton~=oldButton) then
      self.changeCounter=0.1  -- repeat rate
      playSound(menuMove)
    end
  end
  if (p1) then
    if (p1:isGamepadDown('a')) then
      self.buttons[self.selectedButton]:activate()
    end
  end
end

function MenuList:keypressed(key, code)
  local oldButton = self.selectedButton
  if (key=='up') or (key=='w') then
    self.selectedButton=self.selectedButton-1
  end
  if (key=='down') or (key=='s') then
    self.selectedButton=self.selectedButton+1
  end
  
  if (self.selectedButton<1) then
      self.selectedButton=1
  elseif (self.selectedButton>self.maxButtons) then
      self.selectedButton=self.maxButtons
  end
  
  if (self.selectedButton~=oldButton) then
    playSound(menuMove)
  end
    
  if (key=='return') or (key=='e') then
    self.buttons[self.selectedButton]:activate()
  end
end

return MenuList