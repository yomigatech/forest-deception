local class = require 'lib/middleclass'

-- settings for the panels
local width = 240
local height = 128
local spacing = 20
local startingWill = 200

-- Willpower bar (internal class)
local Willpower = class('Willpower')

-- private methods
local function getBoundingBox(self)
  local x = 1016
  local y = 150 + (self.position - 1) * (height + spacing)

  return x,y,width,height
end

-- public methods
function Willpower:initialize(y,maxValue)
  self.y = y
  self.x = 1016
  self.maxValue = maxValue
  self.curValue = maxValue
end

function Willpower:draw()
  local colorRGB={ {0x00,0xff,0x00},{0xca,0xff,0x00},{0xee,0xff,0x00},{0xee,0xc8,0x00},
                   {0xee,0x8d,0x00},{0xee,0x21,0x00},{0x84,0x84,0xef} }
  local percent = self.curValue/self.maxValue
  local idx = math.ceil(percent*7)
  if (idx<1) then
    idx=1
  end
  local h=20
  
  love.graphics.setColor(0,0,0)
  love.graphics.rectangle('fill', self.x+8, self.y, width-16, h)
  love.graphics.setColor(colorRGB[8-idx][1],colorRGB[8-idx][2],colorRGB[8-idx][3])
  love.graphics.rectangle('fill', self.x+8+1, self.y+1, percent * (width-16)-2, h-2)
  love.graphics.setColor(255,255,255)
  love.graphics.rectangle('line', self.x+8, self.y, width-16, h)
  love.graphics.setColor(0,0,0)
  love.graphics.printf("WillPower",self.x+8,self.y+4,width-16,'center')
end

-- Panel (internal class)
local Panel = class('Panel')

-- public methods
function Panel:initialize(position,label,callback)
  self.position = position
  self.callback = callback
  self.label = label
  self.resources = 3
  local x, y, width, height = getBoundingBox(self)
  self.willpower = Willpower:new(y+32,startingWill)
end

function Panel:setResources(resources) 
  self.resources=resources
end

function Panel:addWill(num) 
  local total=self.willpower.curValue+num
  if (total>self.willpower.maxValue) then
      total=self.willpower.maxValue
  end
  self.willpower.curValue=total
end
  
function Panel:draw()
  local x, y, width, height = getBoundingBox(self)
  love.graphics.setColor(32,128,32)
  love.graphics.rectangle('fill',x,y,width,height)
  love.graphics.setColor(192,255,192)
  love.graphics.rectangle('line',x,y,width,height)

  love.graphics.setNewFont(12)
  self.willpower:draw()
  love.graphics.setColor(255,255,255)
  love.graphics.setFont(smallFont)
  love.graphics.print("P"..self.position..": "..self.label,x+8,y+8)
  
  if (self.resources>0) then
    love.graphics.setColor(192,255,192)
  else
    love.graphics.setColor(64,64,64)
  end
  love.graphics.circle('fill',x+24,y+height-52,16)
  if (self.resources>1) then
    love.graphics.setColor(192,255,192)
  else
    love.graphics.setColor(64,64,64)
  end
  love.graphics.circle('fill',x+24+48,y+height-52,16)
  if (self.resources>2) then
    love.graphics.setColor(192,255,192)
  else
    love.graphics.setColor(64,64,64)
  end
  love.graphics.circle('fill',x+24+96,y+height-52,16)
  
  love.graphics.setColor(0,0,0)
  love.graphics.circle('line',x+24+96,y+height-52,16)
  love.graphics.circle('line',x+24+48,y+height-52,16)
  love.graphics.circle('line',x+24,y+height-52,16)
end

function Panel:update(dt)
  local scale={0,1,2,4}
  if (self.willpower.curValue>0) then
    self.willpower.curValue=self.willpower.curValue-dt*scale[4-self.resources]
    if (self.willpower.curValue<=0) then
      activePartyMembers=activePartyMembers-1
    end
  end
end

-- PanelStack
local StatusPanels = class('StatusPanels')

-- private methods
local function parseButtonOptions(self, buttonOptions)
  local options, label, callback
  self.maxButtons = 0
  for i=1, #buttonOptions do
    options = buttonOptions[i]
    label = options[1]
    callback = options[2]
    table.insert(self.buttons, Panel:new(i, label, callback))
    self.maxButtons = self.maxButtons + 1
  end
end

--public functions
function StatusPanels:initialize(buttonOptions)
  self.buttons = {}
  parseButtonOptions(self, buttonOptions)
end

function StatusPanels:draw()
  for i=1, #self.buttons do
    self.buttons[i]:draw()
  end
end

function StatusPanels:update(dt)
  for i=1, #self.buttons do
    self.buttons[i]:update(dt)
  end
end

function StatusPanels:addResource(player) 
  if (self.buttons[player]~=nil) then
    local num=self.buttons[player].resources
    if (num<3) then
      self.buttons[player]:setResources(num+1)
    end
  end
end

function StatusPanels:remResource(player) 
  if (self.buttons[player]~=nil) then
    local num=self.buttons[player].resources
    if (num>0) then
      self.buttons[player]:setResources(num-1)
    end
  end
end

function StatusPanels:addWill(player,num) 
  if (self.buttons[player]~=nil) then
    self.buttons[player]:addWill(num)
  end
end

return StatusPanels