local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
require 'menu'
require 'forest'
require 'switch'
require 'select'
require 'howto'
require 'credits'
require 'options'
require 'gamewin'
require 'gameover'

local game

function love.load(arg)
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  game = Game:new()
end

function love.update(dt)
  game:update(dt)
end

function love.draw()
  game:draw()
end

function love.keypressed(key, code)
  game:keypressed(key, code)
end
