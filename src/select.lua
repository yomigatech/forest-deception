local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
local CharList = require 'charlist'

local SelectChar = Game:addState("SelectChar")

function SelectChar:enteredState() -- create buttons, options, etc and store them into self
  self.screenWidth = 1280
  self.numPlayers = 0
  gameStart = 0
  
  local characters = {
    { 'Nobleman', face = 1, body = nil },
    { 'Lumberjack', face = 2, body = nil },
    { 'Bard',  face = 3, body = nil },
    { 'Time Traveller', face = 4, body = nil },
    { '????', face = 5, body = nil }
  }
  self.chars = CharList:new(characters)
  
  --annoying problem of holding down 'A' button from previous menu
  p1Debounce=false
  if (p1) and (p1:isGamepadDown('a')) then
    p1Debounce=true
  end
end

function SelectChar:draw() -- draw the menu
  love.graphics.setBackgroundColor(128,128,128)
  
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Select Characters",0,100,1280,'center')
  
  if (gameStart>0) then
    local sec = math.floor(gameStart)+1
    if (sec>1) then
      love.graphics.printf("Game starting in "..math.floor(gameStart).." seconds",0,150,1280,'center')
    elseif (sec==1) then
      love.graphics.printf("Game starting in 1 second",0,150,1280,'center')
    end
  end

  love.graphics.setFont(smallFont)
  self.chars:draw()
  love.graphics.setColor(255,255,64)
  love.graphics.setFont(menuFont)
  
  if (self.numPlayers<expectedPlayers) then
    love.graphics.print('...waiting for all players to join...', 550, 600)
    love.graphics.print("Please plug in additional controllers", 550,640)
  end
    
end

function SelectChar:update(dt) -- update anything that needs updates
  self.joystickCount = love.joystick.getJoystickCount()
  self.numPlayers = self.joystickCount
  if (keyboardPlayer>0) then
    self.numPlayers = self.numPlayers+1
  end
  if (p1) and (p1Debounce) and (p1:isGamepadDown('a')==false) then
    p1Debounce=false 
  end
  
  self.chars:update(dt)
  
  if (gameStart>0) then
    gameStart=gameStart-dt
    if (gameStart<=0) then
      self:gotoState('Forest')
    end
  end  
end

function SelectChar:exitedState() -- destroy buttons, options etc here
  self.chars=nil
end

function SelectChar:keypressed(key, code)
  if (key == 'escape') then
    self:popState()
    playSound(menuBack)
  else
    self.chars:keypressed(key,code)
  end
end

return SelectChar