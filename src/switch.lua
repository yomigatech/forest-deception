local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
local Forest = require 'forest'

local Switch = Game:addState("Switch")

function Switch:enteredState() -- create buttons, options, etc and store them into self

end

function Switch:update()

end

function Switch:draw()
	love.timer.sleep(3)
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill",180,60,640,480)
	love.graphics.setColor(255,255,255)
	love.timer.sleep(3)
	self:gotoState("Forest")
end

