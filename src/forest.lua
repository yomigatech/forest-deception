local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
local StatusPanel = require 'status'
local Item = require 'item'

local Forest = Game:addState("Forest")

function Forest:enteredState()
  ambient:play()
  scaler = 4
  screenwidth = 1280
  screenheight = 720
  displaywidth = (640/scaler)
  displayheight = (480/scaler)
  player1.x = 150
  player1.y = 90
  player2.x = 170
  player2.y = 90
  p1time = 0
  p1timeNow = 0
  p1timeSince = 0
  p2time = 0
  p2timeNow = 0
  p2timeSince = 0
  moveNextArea = false
  winner = "none"
  if (p3) then
    player3.x = 190
    player3.y = 90
    p3zone = "none"
    p3pressed = false
    p3confirmed = "none"
    p3time = 0
    p3timeNow = 0
    p3timeSince = 0
  end
  p1zone = "none"
  p2zone = "none"
  p1pressed = false
  p2pressed = false
  p1confirmed = "none"
  p2confirmed = "none"
  first = "none"
  zoneconfirmed = "none"
  resources = {nil,nil,nil,nil,nil}
  map = nil
  number = nil
  createMap()
  itemNumberx = 0
  itemNumbery = 0
  
  activePartyMembers = 0
  
  if (p3) then
    self.status = StatusPanel:new({
      { player1.name, function() self:pushState('SelectChar') end },
      { player2.name, function() self:pushState('SelectChar') end },
      { player3.name, function() self:pushState('SelectChar') end }
    })
    activePartyMembers = 3
    player1.alive=true
    player2.alive=true
    player3.alive=true
	else
	self.status = StatusPanel:new({
      { player1.name, function() self:pushState('SelectChar') end },
      { player2.name, function() self:pushState('SelectChar') end }
    })
    activePartyMembers = 2
    player1.alive=true
    player2.alive=true
    player3.alive=false
  end

	function resRemove()
		self.status:remResource(1)
		self.status:remResource(2)
		if (p3) then
			self.status:remResource(3)
		end
	end

end

function createMap()
  items = {}
	tile = {}

	tile[1] = love.graphics.newImage("assets/cellImage_Forest_0001.png")
	tile[2] = love.graphics.newImage("assets/cellImage_Forest_0002.png")
  tile[3] = love.graphics.newImage("assets/cellImage_Forest_0003.png")
  tile[4] = love.graphics.newImage("assets/cellImage_Forest_0004.png")
  tile[5] = love.graphics.newImage("assets/cellImage_Forest_0005.png")
  tile[6] = love.graphics.newImage("assets/cellImage_Forest_0006.png")

	number = love.math.random(1,6)
	map	= tile[number]
  map:setFilter("nearest")
	
	-- create resources
	itemNumberx = love.math.random(10,22)*10
  itemNumbery = love.math.random(4,12)*10
  itemType = love.math.random(1,6)
  items[1]=Item:new(itemNumberx,itemNumbery,itemType)
  
  if (love.math.random(1,100)>50) then
    itemNumberx = love.math.random(10,22)*10
    itemNumbery = love.math.random(4,12)*10
    itemType = love.math.random(1,6)
    items[2]=Item:new(itemNumberx,itemNumbery,itemType)
  else 
    items[2]=nil
  end
  
  if (love.math.random(1,100)>75) then
    itemNumberx = love.math.random(10,22)*10
    itemNumbery = love.math.random(4,12)*10
    itemType = love.math.random(1,6)
    items[3]=Item:new(itemNumberx,itemNumbery,itemType)
  else
    items[3]=nil
  end
  
end

function drawMap()
	love.graphics.draw(map,80,30,0,0.75)
  for i=1,3 do
    if (items[i]) then items[i]:draw() end
  end
end

function Forest:draw()
	love.graphics.setColor(255,255,255)
  self.status:draw()
  
	love.graphics.setNewFont(12)
	love.graphics.scale(scaler,scaler)
	love.graphics.setBackgroundColor(128,128,128,0)
	love.graphics.setColor(255,255,255)
	drawMap()
 
  spriteSheets[player1.spriteIdx].anim[player1.animIdx]:draw(spriteSheets[player1.spriteIdx].image,player1.x,player1.y, 0, player1.flipX * 0.75, 0.75, 6, 0)
  spriteSheets[player2.spriteIdx].anim[player2.animIdx]:draw(spriteSheets[player2.spriteIdx].image,player2.x,player2.y, 0, player2.flipX * 0.75, 0.75, 6, 0)
  if (p3) then
  spriteSheets[player3.spriteIdx].anim[player3.animIdx]:draw(spriteSheets[player3.spriteIdx].image,player3.x,player3.y, 0, player3.flipX * 0.75, 0.75, 6, 0)
  end
	
	if (player1.x > 200 and player1.y > 40 and player1.y < 120) then
		love.graphics.setColor(128,255,128,128-(320-player1.x))
		love.graphics.rectangle("fill",228,30,20,120)
		p1zone = "east"
		if p1:isGamepadDown('a') then	
			p1pressed = true
			p1press()
			p1time = love.timer.getTime()
		end

	elseif (player1.x < 100 and player1.y > 40 and player1.y < 120) then
		love.graphics.setColor(128,255,128,128-(320+player1.x))
		love.graphics.rectangle("fill",80,30,20,120)
		p1zone = "west"
		if p1:isGamepadDown('a') then
			p1pressed = true
			p1press()
			p1time = love.timer.getTime()
		end
	
	elseif (player1.y < 50 and player1.x < 228 and player1.x > 100) then
		love.graphics.setColor(128,255,128,128-(320-player1.y))
		love.graphics.rectangle("fill",80,30,168,20)
		p1zone = "north"
		if p1:isGamepadDown('a') then
			p1pressed = true
			p1press()
			p1time = love.timer.getTime()
		end
	
	elseif (player1.y > 120 and player1.x < 280 and player2.x > 40) then
		love.graphics.setColor(128,255,128,128-(320-player1.y))
		love.graphics.rectangle("fill",80,130,168,20)
		p1zone = "south"
		if p1:isGamepadDown('a') then
			p1pressed = true
			p1press()
			p1time = love.timer.getTime()
		end
	else
		p1zone = "none"		
		p1time = 0
	end

 if (player2.x > 200 and player2.y > 40 and player2.y < 120) then
        love.graphics.setColor(128,255,128,128-(320-player2.x))
        love.graphics.rectangle("fill",228,30,20,120)
        p2zone = "east"
        if p2:isGamepadDown('a') then
            p2pressed = true
            p2press()
			p2time = love.timer.getTime()	
        end

    elseif (player2.x < 100 and player2.y > 40 and player2.y < 120) then
        love.graphics.setColor(128,255,128,128-(320+player2.x))
        love.graphics.rectangle("fill",80,30,20,120)
        p2zone = "west"
        if p2:isGamepadDown('a') then
            p2pressed = true
            p2press()
			p2time = love.timer.getTime()
        end

	 elseif (player2.y < 50 and player2.x < 228 and player2.x > 100) then
        love.graphics.setColor(128,255,128,128-(320-player2.y))
        love.graphics.rectangle("fill",80,30,168,20)
        p2zone = "north"
        if p2:isGamepadDown('a') then
            p2pressed = true
            p2press()
			p2time = love.timer.getTime()
        end

    elseif (player2.y >120 and player2.x < 228 and player2.x > 100) then
        love.graphics.setColor(128,255,128,128-(320-player2.y))
        love.graphics.rectangle("fill",80,130,168,20)
        p2zone = "south"
        if p2:isGamepadDown('a') then
            p2pressed = true
            p2press()
			p2time = love.timer.getTime()
        end
    else
        p2zone = "none"
		p2time = 0
    end

  if (p3) then
    if (player3.x > 200 and player3.y > 40 and player3.y < 120) then
      love.graphics.setColor(128,255,128,128-(320-player3.x))
      love.graphics.rectangle("fill",228,30,20,120)
      p3zone = "east"
      if p3:isGamepadDown('a') then
        p3pressed = true
        p3press()
        p3time = love.timer.getTime()
      end
    elseif (player3.x < 100 and player3.y > 40 and player3.y < 120) then
      love.graphics.setColor(128,255,128,128-(320+player3.x))
      love.graphics.rectangle("fill",80,30,20,120)
      p3zone = "west"
      if p3:isGamepadDown('a') then
        p3pressed = true
        p3press()
			p3time = love.timer.getTime()
      end
    elseif (player3.y < 50 and player3.x < 228 and player3.x > 100) then
      love.graphics.setColor(128,255,128,128-(320-player3.y))
      love.graphics.rectangle("fill",80,30,168,20)
      p3zone = "north"
      if p3:isGamepadDown('a') then
        p3pressed = true
        p3press()
        p3time = love.timer.getTime()
      end
    elseif (player3.y >120 and player3.x < 228 and player3.x > 100) then
      love.graphics.setColor(128,255,128,128-(320-player3.y))
      love.graphics.rectangle("fill",80,130,168,20)
      p3zone = "south"
      if p3:isGamepadDown('a') then
        p3pressed = true
        p3press()
        p3time = love.timer.getTime()
      end
    else
      p3zone = "none"
      p3time = 0
    end
  end
end

function Forest:update(dt) -- update anything that needs updates
  for i=1,3 do
    if (items[i]) then 
      items[i]:update(dt) 
      if (items[i].pickUp>0) then
        if (items[i].idx==6) then
          self.status:addWill(items[i].pickUp,25)
        else
          self.status:addResource(items[i].pickUp)
        end
        playSound(itemPickup)
        items[i].pickUp=0
      end
    end
  end
 
  self.status:update(dt)
  if (activePartyMembers < 2) then
    self:gotoState("GameOver")
  end
    
	p1timeNow = love.timer.getTime()
	p1timeSince = p1timeNow - p1time
	p2timeNow = love.timer.getTime()
	p2timeSince = p2timeNow - p2time
	if (p3) then
		p3timeNow = love.timer.getTime()
		p3timeSince = p3timeNow - p3time
	end
  
  if (player1.animate) then
    spriteSheets[player1.spriteIdx].anim[player1.animIdx]:update(dt)
  end
  if (player2.animate) then
    spriteSheets[player2.spriteIdx].anim[player2.animIdx]:update(dt)
  end
  if (player3.animate) then
    spriteSheets[player3.spriteIdx].anim[player3.animIdx]:update(dt)
  end
  
	local deadzone1 = 0.06 -- 0.25 * 0.25
	local left1x = p1:getGamepadAxis('leftx')
	local left1y = p1:getGamepadAxis('lefty')
	local magnitude1 = left1x*left1x+left1y*left1y
	local player1speed = 2
	if(magnitude1<deadzone1) then
		left1x = 0
		left1y = 0
    player1.anim=1 -- idle
    player1.animate=false -- but don't update for now
	end
  
  if (left1x<0) then
    player1.flipX=-1
    player1.anim=2  -- side view
    player1.animate=true -- and animate
  elseif (left1x>0) then
    player1.flipX=1
    player1.anim=2
    player1.animate=true
  end

	player1.x = player1.x + (left1x * player1speed)
	player1.y = player1.y + (left1y * player1speed)

  if (p2) then 
    local deadzone2 = 0.06 -- 0.25 * 0.25
    local left2x = p2:getGamepadAxis('leftx')
    local left2y = p2:getGamepadAxis('lefty')
    local magnitude2 = left2x*left2x+left2y*left2y
    local player2speed = 2
    if(magnitude2<deadzone2) then
      left2x = 0
      left2y = 0
      player2.anim=1 -- idle
      player2.animate=false -- but don't update for now
    end
    
    if (left2x<0) then
      player2.flipX=-1
      player2.anim=2  -- side view
      player2.animate=true -- and animate
    elseif (left2x>0) then
      player2.flipX=1
      player2.anim=2
      player2.animate=true
    end
  
    player2.x = player2.x + (left2x * player2speed)
    player2.y = player2.y + (left2y * player2speed)
  end
  
  if (p3) then
    local deadzone3 = 0.06 -- 0.25 * 0.25
    local left3x = p3:getGamepadAxis('leftx')
    local left3y = p3:getGamepadAxis('lefty')
    local magnitude3 = left3x*left3x+left3y*left3y
    local player3speed = 2
    if (magnitude3<deadzone3) then
      left3x = 0
      left3y = 0
      player2.anim=1 -- idle
      player2.animate=false
    end
    
    if (left3x<0) then
      player3.flipX=-1
      player3.anim=2  -- side view
      player3.animate=true -- and animate
    elseif (left3x>0) then
      player3.flipX=1
      player3.anim=2
      player3.animate=true
    end
    
    player3.x = player3.x + (left3x * player3speed)
    player3.y = player3.y + (left3y * player3speed)
  end

	function p1press()
    if (p1confirmed~=p1zone) then
      local voice=1
      if (p1zone=='north') then
        playSound(direction[voice].north)
      elseif (p1zone=='south') then
        playSound(direction[voice].south)
      elseif (p1zone=='east') then
        playSound(direction[voice].east)
      elseif (p1zone=='west') then
        playSound(direction[voice].west)
      end
      p1confirmed = p1zone
    end
	end

	function p2press()
    if (p2confirmed~=p2zone) then
      local voice=1
      if (p2zone=='north') then
        playSound(direction[voice].north)
      elseif (p2zone=='south') then
        playSound(direction[voice].south)
      elseif (p2zone=='east') then
        playSound(direction[voice].east)
      elseif (p2zone=='west') then
        playSound(direction[voice].west)
      end
      p2confirmed = p2zone
    end
	end

	if (p3) then
		function p3press()
      if (p3confirmed~=p3zone) then
        local voice=1
        if (p3zone=='north') then
          playSound(direction[voice].north)
        elseif (p3zone=='south') then
          playSound(direction[voice].south)
        elseif (p3zone=='east') then
          playSound(direction[voice].east)
        elseif (p3zone=='west') then
          playSound(direction[voice].west)
        end
        p3confirmed = p3zone
      end
		end
	end


	if (p3) then	
		if p1confirmed == p2confirmed and p2confirmed == p3confirmed then
			zoneconfirmed = p1confirmed
			if p1timeSince > p2timeSince then
				if p1timeSince > p3timeSince then
					winner = "p1"
				else
					winner = "p3"
				end
			elseif p2timeSince > p3timeSince then
				winner = "p2"
			else
				winner = "p3"
			end
			love.timer.sleep(1)
            playSound(partyconfirm)
			self:gotoState('Switch')			
		end
	else
		if (p1confirmed == p2confirmed) and (p1zone ~= "none") and  (p2zone ~= "none") then
			zoneconfirmed = p1confirmed
			if p1timeSince > p2timeSince then
				winner = "p1"
			else
				winner = "p2"
			end
            love.timer.sleep(1)
            playSound(partyConfirm)
			self:gotoState('Switch')



		end
	end
end

function Forest:exitedState() -- destroy buttons, options etc here
  --print("exiting menu state")
end

function Forest:keypressed(key, code)
  if (key == 'escape') then
    self:gotoState('GameOver')
  elseif (key == ' ') then
    self.status:remResource(1)
	self.status:remResource(2)
		if (p3) then
			self.status:remResource(3)
		end
  elseif (key == 'e') then
    self.status:addResource(1)
	self.status:addResource(2)
		if (p3) then
			self.status:remResource(3)
		end
  elseif (key == 'r') then
    self.status:addWill(1,25)
  end
end

return Forest
