local class = require 'lib/middleclass'

local Item = class('Item')

--public functions
function Item:initialize(x,y,itemIdx)
  self.x = x
  self.y = y
  self.idx = itemIdx
  self.w = spriteSheets[self.idx].itemW
  self.h = spriteSheets[self.idx].itemH
  self.active = true
  self.pickUp = 0
end

function Item:draw()
  if (self.active) then
    spriteSheets[self.idx].itemAnim:draw(spriteSheets[self.idx].item,self.x,self.y, 0, 0.75, 0.75)
  end
end

function Item:update(dt)
  if (self.active) then
    spriteSheets[self.idx].itemAnim:update(dt)
    local hit = false
    local playerIdx = 0
    if (player1.spriteIdx==self.idx) or (self.idx==6) then 
      hit = not ((player1.x > self.x+self.w) or
              (player1.x+player1.w < self.x) or
              (player1.y > self.y+self.h) or
              (player1.y+player1.h < self.y))
      playerIdx=1
    elseif (player2.spriteIdx==self.idx) or (self.idx==6) then
      hit = not ((player2.x > self.x+self.w) or
              (player2.x+player2.w < self.x) or
              (player2.y > self.y+self.h) or
              (player2.y+player2.h < self.y))
      playerIdx=2
    elseif (p3) and (player3.spriteIdx==self.idx) or (self.idx==6) then
      hit = not ((player3.x > self.x+self.w) or
              (player3.x+player3.w < self.x) or
              (player3.y > self.y+self.h) or
              (player3.y+player3.h < self.y))
      playerIdx=3
    end
    
    if (hit) then
      self.active=false
      self.pickUp=playerIdx
    end
    
  end
end

return Item
