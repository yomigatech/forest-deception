local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'
local MenuList = require 'menulist'

local Menu = Game:addState("Menu")

function Menu:enteredState() -- create buttons, options, etc and store them into self
  --print("entering menu state")
  self.screenWidth = 1280
  love.graphics.setFont(menuFont)
  menuSong:play() 
  self.menu = MenuList:new({
      { 'Start Game', function() self:pushState('SelectChar') end },
      { 'How to Play', function() self:pushState('HowTo') end },
     -- { 'Options', function() self:pushState('Options') end },
      { 'Credits', function() self:pushState('Credits') end },
      { 'Exit', function() love.event.push('quit') end }
      })
end

function Menu:draw() -- draw the menu
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.draw(self.title, (self.screenWidth-self.title:getWidth())/2, 100)
  self.menu:draw()
  love.graphics.setColor(255,255,64)
end

function Menu:update(dt) -- update anything that needs updates
  self.menu:update(dt)
end

function Menu:exitedState() -- destroy buttons, options etc here
  self.menu=nil;
end

function Menu:keypressed(key, code)
  if (key == 'escape') then
    love.event.push('quit')
  else
    self.menu:keypressed(key, code)
  end
end

return Menu
