local class = require 'lib/middleclass'

-- PORTRAIT (internal class)
local Portrait = class('Portrait')

-- settings for all portraits
local width = 136
local height = 136
local spacing = 20 -- horizontal spacing

-- private methods
local function getBoundingBox(self)
  local x = 260 + (self.position - 1) * (width + spacing)
  local y = love.graphics.getHeight() / 2 - height / 2
  return x,y,width,height
end

-- public methods
function Portrait:initialize(position,label,face)
  self.position = position
  self.label = label
  self.face = face
end

function Portrait:draw(p1Sel, p2Sel, p3Sel, p1Done, p2Done, p3Done)
  local x, y, width, height = getBoundingBox(self)
  if (expectedPlayers==2) then
    p3Sel = false
    p3Done = false
  end
  
  if (p1Sel) or (p2Sel) or (p3Sel) then
    love.graphics.setColor(192,192,192)
  else
    love.graphics.setColor(255,255,255)
  end 
  if (p1Done) then
    love.graphics.setColor(255,64,64)
  elseif (p2Done) then 
    love.graphics.setColor(64,255,64)
  elseif (p3Done) then
    love.graphics.setColor(64,64,255)
  end
  love.graphics.rectangle('fill', x, y, width, height)
  
  if (p1Sel) or (p2Sel) or (p3Sel) then
    love.graphics.setColor(192,192,192)
  else
    love.graphics.setColor(255,255,255)
  end 
  love.graphics.draw(portraits[self.face],x+4,y+4)
  love.graphics.printf(self.label, x, y+height+4, width, 'center')
  love.graphics.setColor(0,0,0)
  love.graphics.rectangle('line', x, y, width, height)
  
  if (p1Sel) then
    love.graphics.setColor(255,64,64)
    love.graphics.draw(pointers[1],x+5,y-36)
  end
  if (p2Sel) then
    love.graphics.setColor(64,255,64)
    love.graphics.draw(pointers[2],x+42+5,y-36)
  end
  if (p3Sel) and (expectedPlayers>2) then
    love.graphics.setColor(64,64,255)
    love.graphics.draw(pointers[3],x+84+5,y-36)
  end
end

-- MENU
local CharList = class('CharList')

-- private methods
local function parsePortraitOptions(self, portraitOptions)
  local options, label, face
  self.maxPortraits = 0
  for i=1, #portraitOptions do
    options = portraitOptions[i]
    label = options[1]
    face = options.face
    table.insert(self.portraits, Portrait:new(i, label, face))
    self.maxPortraits = self.maxPortraits + 1
  end
end

--public functions
function CharList:initialize(portraitOptions)
  self.portraits = {}
  parsePortraitOptions(self, portraitOptions)
  self.hoverPortrait = {1,1,1}
  self.changeCounter = {0,0,0}
  self.selectedPortrait = {0,0,0}
  self.totalSelected = 0
end

function CharList:draw()
  for i=1, #self.portraits do
    self.portraits[i]:draw(self.hoverPortrait[1]==i,self.hoverPortrait[2]==i,self.hoverPortrait[3]==i,
      self.selectedPortrait[1]==i,self.selectedPortrait[2]==i,self.selectedPortrait[3]==i)
  end
end

function CharList:update(dt)
  -- a bit weird here, since we are emulating key repeat
  local oldPortrait = { self.hoverPortrait[1], self.hoverPortrait[2], self.hoverPortrait[3] }
  
  local pn = nil
  local i=1
  
  while(i~=expectedPlayers+1) do
    if (i==1) then
      pn=p1
    elseif (i==2) then
      pn=p2
    elseif (i==3) then
      pn=p3
    end
  
    -- allow 'b' to unselect
    if (pn) then
      if (pn:isGamepadDown('a')==false) and (self.selectedPortrait[i]~=0) and (pn:isGamepadDown('b')) then
        self:deselectPortrait(i)
      end
    end
    
    -- if we have a selection, don't allow us to move
    if (self.selectedPortrait[i]~=0) then
      pn=nil
    end
    
    -- move left/right if counter allows
    if (self.changeCounter[i]>0) then
      self.changeCounter[i]=self.changeCounter[i]-dt
    else
      if (pn) then
        local leftx = pn:getGamepadAxis('leftx')
        if (leftx<-0.25) then
          self.hoverPortrait[i]=self.hoverPortrait[i]-1
        elseif (leftx>0.25) then
          self.hoverPortrait[i]=self.hoverPortrait[i]+1
        end
      end
  
      if (self.hoverPortrait[i]<1) then
        self.hoverPortrait[i]=1
      elseif (self.hoverPortrait[i]>self.maxPortraits) then
        self.hoverPortrait[i]=self.maxPortraits
      end
  
      if (self.hoverPortrait[i]~=oldPortrait[i]) then
        self.changeCounter[i]=0.1  -- repeat rate
        playSound(menuMove)
      end
    end
    
    if (pn) then
      if (pn:isGamepadDown('a')) and (self.selectedPortrait[i]==0) and (pn:isGamepadDown('b')==false) then
        self:selectPortrait(i)
      end
    end
    
    -- for each controller
    i=i+1
    
  end
end

function CharList:selectPortrait(player)
  i=1;
  if (player==1) and (p1Debounce) then
    return
  end
  
  while(i<4) do
    if (self.selectedPortrait[i]==self.hoverPortrait[player]) then
      return
    end
    i=i+1
  end
  self.selectedPortrait[player]=self.hoverPortrait[player]
  self.totalSelected=self.totalSelected+1
  playSound(menuSelect)
  playSound(names[self.selectedPortrait[player]])
  
  if (player==1) then
    player1.spriteIdx=self.selectedPortrait[player]
    player1.name=self.portraits[self.selectedPortrait[player]].label
  elseif (player==2) then
    player2.spriteIdx=self.selectedPortrait[player]
    player2.name=self.portraits[self.selectedPortrait[player]].label
  elseif (player==3) then
    player3.spriteIdx=self.selectedPortrait[player]
    player3.name=self.portraits[self.selectedPortrait[player]].label
  end
  
  if (self.totalSelected==expectedPlayers) then
    gameStart=5
  end
end

function CharList:deselectPortrait(player)
  if (self.selectedPortrait[player]) then
    self.selectedPortrait[player]=0
    self.totalSelected = self.totalSelected - 1
    gameStart = 0
    playSound(menuBack)
  end
end

function CharList:keypressed(key, code)
  if (keyboardPlayer>0) then
    if (self.selectedPortrait[keyboardPlayer]==0) then
      oldSelect=self.hoverPortrait[keyboardPlayer]
      
      if (key=='left') or (key=='a') then
        self.hoverPortrait[keyboardPlayer]=self.hoverPortrait[keyboardPlayer]-1
      end
      if (key=='right') or (key=='d') then
        self.hoverPortrait[keyboardPlayer]=self.hoverPortrait[keyboardPlayer]+1
      end
    end
  
    if (self.hoverPortrait[keyboardPlayer]<1) then
      self.hoverPortrait[keyboardPlayer]=1
    elseif (self.hoverPortrait[keyboardPlayer]>self.maxPortraits) then
      self.hoverPortrait[keyboardPlayer]=self.maxPortraits
    end
    
    if (oldSelect~=self.hoverPortrait[keyboardPlayer]) then
      playSound(menuMove)
    end  
    
    if (key=='return') or (key=='e') then
      if (self.selectedPortrait[keyboardPlayer]~=0) then
        self:deselectPortrait(keyboardPlayer)
      else
        self:selectPortrait(keyboardPlayer)
      end
    end
  end
end

return CharList