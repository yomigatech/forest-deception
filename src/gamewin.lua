local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'

local GameWin = Game:addState("GameWin")

function GameWin:enteredState() -- create buttons, options, etc and store them into self
  -- load close win screen here
  -- and probably win music/fanfare!
end

function GameWin:draw() -- draw the menu
  love.graphics.setFont(menuFont) 
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Congratulations!",0,100,1280,'center')
  
  love.graphics.printf("The Fountain of Fortune lies before you...",0,200,1280,'center')
  -- display win screen graphic
  love.graphics.printf("...by striving together, each of your dreams are about to be fulfilled.",0,600,1280,'center')
  love.graphics.setFont(smallFont)
  love.graphics.setColor(0,0,0)
  love.graphics.printf("Press B button after you are finished celebrating",0,660,1280,'center')
  love.graphics.setFont(menuFont) 
end

function GameWin:update(dt) -- update anything that needs updates
  if (p1) then
    if (self.p1Back) and (p1:isGamepadDown('b')==false) then
      playSound(menuBack)
      self:gotoState("Menu")
    elseif (p1:isGamepadDown('b')) then
      self.p1Back=true
    end
  end
end

function GameWin:exitedState() -- destroy buttons, options etc here
  self.p1Back=false
end

function GameWin:keypressed(key, code)
  if (key == 'escape') then
    playSound(menuBack)
    self:gotoState("Menu")
  end
end

return GameWin