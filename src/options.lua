local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'

local Options = Game:addState("Options")

function Options:enteredState() -- create buttons, options, etc and store them into self
  self.p1Back=false
end

function Options:draw() -- draw the menu
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Options",0,100,1280,'center')
end

function Options:update(dt) -- update anything that needs updates
  if (p1) then
    if (self.p1Back) and (p1:isGamepadDown('b')==false) then
      playSound(menuBack)
      self:popState()
    elseif (p1:isGamepadDown('b')) then
      self.p1Back=true
    end
  end
end

function Options:exitedState() -- destroy buttons, options etc here
  self.p1Back=false
end

function Options:keypressed(key, code)
  if (key == 'escape') then
    playSound(menuBack)
    self:popState()
  end
end

return Options