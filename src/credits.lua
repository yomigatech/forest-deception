local class = require 'lib/middleclass'
local Stateful = require 'lib/stateful'
local Game = require 'game'

local Credits = Game:addState("Credits")

function Credits:enteredState() -- create buttons, options, etc and store them into self
  self.p1Back=false
  self.ggjLogo=love.graphics.newImage("assets/ggj.png")
  self.loveLogo=love.graphics.newImage("assets/love.png")
end

function Credits:draw() -- draw the menu
  love.graphics.setBackgroundColor(128,128,128)
  love.graphics.setColor(255,255,255)
  love.graphics.printf("Credits",0,100,1280,'center')
  
  love.graphics.draw(self.title, (self.screenWidth-self.title:getWidth())/2, 200)
  love.graphics.setFont(smallFont)
  love.graphics.draw(self.loveLogo, (426-self.loveLogo:getWidth())/2, 600)
  love.graphics.draw(self.ggjLogo, 852+(426-self.ggjLogo:getWidth())/2, 577)
  love.graphics.printf("Written for Global Game Jam 2015",0,635,1280,'center')
  love.graphics.setFont(menuFont) 
  
  -- presumably more credits here, edit as you see fit!
  love.graphics.setColor(0,0,0)
  love.graphics.printf("Programming and Artwork by:",0,400,1280,'center')
  love.graphics.printf("Elisa Marchione",0,440,1280,'center')
  love.graphics.printf("Yoran Marchione",0,480,1280,'center')
  love.graphics.printf("Mark Schmelzenbach",0,520,1280,'center')
  
  -- do we need to add license info as well?
  
end

function Credits:update(dt) -- update anything that needs updates
  if (p1) then
    if (self.p1Back) and (p1:isGamepadDown('b')==false) then
      playSound(menuBack)
      self:popState()
    elseif (p1:isGamepadDown('b')) then
      self.p1Back=true
    end
  end
end

function Credits:exitedState() -- destroy buttons, options etc here
  self.p1Back=false
  self.ggjLogo=nil
  self.loveLogon=nil
end

function Credits:keypressed(key, code)
  if (key == 'escape') then
    playSound(menuBack)
    self:popState()
  end
end

return Credits