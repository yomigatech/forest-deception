function love.conf(t)
	t.window.title = "Forest of Deception" -- The window title (string)
	t.window.icon = nil                -- Filepath to an image to use as the window's icon (string)
	t.window.width = 1280              -- The window width (number)
	t.window.height = 720              -- The window height (number)
end
